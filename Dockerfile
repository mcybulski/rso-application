FROM	ubuntu:14.04

RUN sudo apt-get -y update
RUN sudo apt-get -y install curl
#innstall gradle
RUN sudo apt-get install -y software-properties-common
RUN sudo add-apt-repository ppa:cwchien/gradle
RUN sudo apt-get -y update
RUN sudo apt-get install -y gradle

RUN add-apt-repository ppa:webupd8team/java
RUN apt-get -y update
# Accept the Oracle Java license
RUN echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 boolean true" | debconf-set-selections
# Install Oracle Java
RUN apt-get -y install oracle-java8-installer

#copy source filse
COPY src /rso-app/src
COPY test-json /rso-app/test-json
COPY build.sh /rso-app/build.sh
COPY run.sh /rso-app/run.sh
COPY build.gradle /rso-app/build.gradle

EXPOSE 8080
# build and run application

CMD cd rso-app; pwd; bash -C './build.sh'; bash -C './run.sh';


