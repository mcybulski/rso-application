#!/bin/sh

sudo docker run --name=rso-load-balancer -d -p 80:80 -v ~/work/studia-projects/RSO/rso-application/load-balancer/sites-enabled:/etc/nginx/sites-enabled -v ~/work/studia-projects/RSO/rso-application/load-balancer/logs:/var/log/nginx load-balancer
