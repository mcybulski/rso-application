#!/bin/sh

sudo docker run --name=rso-load-balancer -t -i -p 80:80 -p 443:443 -v ~/work/studia-projects/RSO/rso-application/load-balancer/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg:ro load-balancer
