package pl.edu.pw.elka.rso;

import com.google.common.collect.Maps;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.edu.pw.elka.rso.api.json.client.ClientErrorResponseJSON;
import pl.edu.pw.elka.rso.api.json.exceptions.RsoAuthenticationException;
import pl.edu.pw.elka.rso.api.json.exceptions.RsoBadRequestException;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Kontroler służący do debugowania połączeń. Zwraca parametry zapytania, które do niego dotarło.
 *
 * @author Michał Cybulski
 */
@RestController
public class ConnectionTestController {

    @RequestMapping("/")
    public String getDefaultHandler() {
        return "OK";
    }

    @RequestMapping("/requestParameters")
    public Map<String, Object> getRequestParameters(HttpServletRequest httpServletRequest) {
        Map<String, Object> result = Maps.newHashMap();

        result.put("remoteAddress", httpServletRequest.getRemoteAddr());
        result.put("remoteHost", httpServletRequest.getRemoteHost());
        result.put("remotePort", httpServletRequest.getRemotePort());
        result.put("method", httpServletRequest.getMethod());
        result.put("pathInfo", httpServletRequest.getPathInfo());
        result.put("localAddress", httpServletRequest.getLocalAddr());
        result.put("localPort", httpServletRequest.getLocalPort());

        return result;
    }
}
