package pl.edu.pw.elka.rso.api;

import com.google.common.base.Preconditions;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.edu.pw.elka.rso.api.json.client.*;
import pl.edu.pw.elka.rso.api.json.data.*;
import pl.edu.pw.elka.rso.api.json.exceptions.RsoAuthenticationException;
import pl.edu.pw.elka.rso.api.json.exceptions.RsoBadRequestException;
import pl.edu.pw.elka.rso.api.json.exceptions.RsoDataHostConnectionException;
import pl.edu.pw.elka.rso.data.DataLayerManagerImpl;
import pl.edu.pw.elka.rso.security.SecurityManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.Serializable;

/**
 * Kontroler obsugujący zapytania związane ze zobowiązaniami.
 *
 * @author Michał Cybulski
 */
@RestController
@RequestMapping("/rso")
public class ObligationsController {

    private static final Logger logger = LoggerFactory.getLogger(ObligationsController.class);

    // parametry związane z bezpieczeństwem
    private static final String CLIENT_ID_PARAM = "uid";
    private static final String TOKEN_PARAM = "token";
    private static final String TIMESTAMP_PARAM = "ts";

    /**
     * Komponent integrujący aplikację z warstwą danych.
     */
    @Autowired
    private DataLayerManagerImpl dataLayerClient;
    @Autowired
    private SecurityManager securityManager;

    /**
     * Zwraca informacje o miesięcznych zobowiązaniach finansowych.
     * Use case nr 1.
     *
     * @param userPesel numer PESEL klienta
     * @return informacje o miesięcznych zobowiązaniach finansowych
     */
    @RequestMapping(value = "/obligations/{userPesel}", method = RequestMethod.GET)
    public ClientGetUserObligationsResponseJSON getUserObligations(
            @PathVariable("userPesel") String userPesel,
            HttpServletRequest httpServletRequest) {
        logger.info("Starting to process getUserObligations");

        Preconditions.checkNotNull(userPesel);
        checkPermissions(httpServletRequest);

        logger.info("Trying to connect to data layer");
        DataGetUserObligationsResponseJSON response = dataLayerClient.getUserObligations(userPesel);
        logger.info("Got response: {}", response);

        ClientGetUserObligationsResponseJSON myResponse = new ClientGetUserObligationsResponseJSON();
        myResponse.setValue(response.getValue());

        if (response.getErrorCode() != null) {
            switch(Integer.parseInt(response.getErrorCode())) {
                case 1:
                    myResponse.setValue(Integer.valueOf(0));
                    break;
                default:
                    logger.error("Data layer returned error code: {}. Throwing exception to notify client.",
                            response.getErrorCode());
                    // wystąpił błąd obsłużony przez warstwę danych
                    throw new RsoDataHostConnectionException("Data layer returned error code: " +
                            response.getErrorCode().toString() +". Please contact our support " +
                            "at telephone number: +48 604-205-246.");
            }
        }

        logger.info("Finished processing request getUserObligations. My response: {}", myResponse);

        return myResponse;
    }

    /**
     * Dodaje nowy kredyt.
     * Use case nr 2.
     *
     * @param clientInsertNewCreditRequestJSON obiekt z informacjami stworzony na podstawie JSON
     * @return rezultat dodawania kredytu
     */
    @RequestMapping(value = "/obligations", method = RequestMethod.POST)
    public ClientInsertNewCreditAnswerJSON insertNewCredit(
            @Valid @RequestBody ClientInsertNewCreditRequestJSON clientInsertNewCreditRequestJSON,
            HttpServletRequest httpServletRequest) {
        logger.info("Starting to process insertNewCredit, got request: {}",
                clientInsertNewCreditRequestJSON);

        Preconditions.checkNotNull(clientInsertNewCreditRequestJSON);
        checkPermissions(httpServletRequest);

        DataInsertNewCreditRequestJSON dataRequest = new DataInsertNewCreditRequestJSON();
        dataRequest.setClient(clientInsertNewCreditRequestJSON.getClient());
        dataRequest.setCredit(clientInsertNewCreditRequestJSON.getCredit());
        dataRequest.setUid(httpServletRequest.getParameter(CLIENT_ID_PARAM));

        HashFunction hashFunction = Hashing.sha512();
        HashCode hashCode = hashFunction.hashObject(dataRequest,
                (from, into) -> into.putUnencodedChars(from.getClient().getFirstName())
                        .putUnencodedChars(from.getClient().getLastName())
                        .putUnencodedChars(from.getClient().getDocumentNumber())
                        .putUnencodedChars(from.getClient().getEmail())
                        .putUnencodedChars(from.getClient().getPesel())
                        .putUnencodedChars(from.getClient().getPhoneNumber())
                        .putUnencodedChars(from.getClient().getAddress().getCity())
                        .putUnencodedChars(from.getClient().getAddress().getStreet())
                        .putUnencodedChars(from.getClient().getAddress().getZipCode())
                        .putUnencodedChars(from.getCredit().getName())
                        .putInt(from.getCredit().getTime())
                        .putInt(from.getCredit().getValue())
        );

        dataRequest.setHash(hashCode.toString());

        logger.info("Trying to connect to data layer. Request body: {}", dataRequest);
        DataInsertNewCreditAnswerJSON response = dataLayerClient.insertNewCredit(dataRequest);
        logger.info("Got response: {}", response);

        if (response.getErrorCode() != null) {
            logger.error("Data layer returned error code: {}. Throwing exception to notify client.",
                    response.getErrorCode());
            // wystąpił błąd obsłużony przez warstwę danych
            throw new RsoDataHostConnectionException("Data layer returned error code: " +
                    response.getErrorCode().toString() +". Please contact our support " +
                    "at telephone number: +48 604-205-246.");
        }

        ClientInsertNewCreditAnswerJSON myResponse = new ClientInsertNewCreditAnswerJSON();
        myResponse.setCreditId(response.getCreditId());

        logger.info("Finished processing insertNewCredit. Returning response: {}", myResponse);

        return myResponse;
    }

    /**
     * Dodaje nową płatność.
     * Use case nr 3.
     *
     * @param clientInsertNewPaymentRequestJSON obiekt z informacjami stworzony na podstawie JSON
     * @param userPesel numer PESEL klienta
     * @return rezultat dodawania płatności
     */
    @RequestMapping(value = "/new-payment/{userPesel}", method = {RequestMethod.POST, RequestMethod.OPTIONS})
    public ClientInsertNewPaymentAnswerJSON insertNewPayment(
            @Valid @RequestBody ClientInsertNewPaymentRequestJSON clientInsertNewPaymentRequestJSON,
            @PathVariable("userPesel") String userPesel,
            HttpServletRequest httpServletRequest) {
        logger.info("Starting to process insertNewPayment. Got request: {}", clientInsertNewPaymentRequestJSON);

        Preconditions.checkNotNull(clientInsertNewPaymentRequestJSON);
        Preconditions.checkNotNull(userPesel);
        checkPermissions(httpServletRequest);

        DataInsertNewPaymentRequestJSON dataInsertNewPaymentRequestJSON = new DataInsertNewPaymentRequestJSON();
        dataInsertNewPaymentRequestJSON.setPayment(clientInsertNewPaymentRequestJSON.getPayment());
        dataInsertNewPaymentRequestJSON.setUid(httpServletRequest.getParameter(CLIENT_ID_PARAM));

        HashFunction hashFunction = Hashing.sha512();
        HashCode hashCode = hashFunction.hashObject(dataInsertNewPaymentRequestJSON,
                (from, into) -> into.putUnencodedChars(from.getUid())
                        .putUnencodedChars(from.getPayment().getCreditId())
                        .putInt(from.getPayment().getValue())
                        .putUnencodedChars(clientInsertNewPaymentRequestJSON.getPaymentName())
        );

        dataInsertNewPaymentRequestJSON.setHash(hashCode.toString());

        logger.info("Trying to connect to data layer. Request body: {}", dataInsertNewPaymentRequestJSON);
        DataInsertNewPaymentAnswerJSON response = dataLayerClient.insertNewPayment(dataInsertNewPaymentRequestJSON,
                userPesel);
        logger.info("Got response: {}", response);

        if (response.getErrorCode() != null) {
            logger.error("Data layer returned error code: {}. Throwing exception to notify client.",
                    response.getErrorCode());
            // wystąpił błąd obsłużony przez warstwę danych
            throw new RsoDataHostConnectionException("Data layer returned error code: " +
                    response.getErrorCode().toString() +". Please contact our support " +
                    "at telephone number: +48 604-205-246.");
        }

        ClientInsertNewPaymentAnswerJSON clientInsertNewPaymentAnswerJSON = new ClientInsertNewPaymentAnswerJSON();
        clientInsertNewPaymentAnswerJSON.setRemaining(response.getRemaining());

        logger.info("Finished processing insertNewCredit. Returning response: {}", clientInsertNewPaymentAnswerJSON);

        return clientInsertNewPaymentAnswerJSON;
    }

    /**
     * Sprawdza czy w requeście zawarte są odpowiednie dane klienta.
     *
     * @param httpServletRequest request
     */
    private void checkPermissions(HttpServletRequest httpServletRequest) {
        String clientId = httpServletRequest.getParameter(CLIENT_ID_PARAM);
        String token = httpServletRequest.getParameter(TOKEN_PARAM);
        String timestampString = httpServletRequest.getParameter(TIMESTAMP_PARAM);

        if (StringUtils.isBlank(clientId) || StringUtils.isBlank(token) || StringUtils.isBlank(timestampString)) {
            throw new RsoBadRequestException("Not all required security headers supplied!");
        }

        long timestamp = Long.parseLong(timestampString);

        if (!securityManager.isTokenValid(token, clientId, timestamp)) {
            throw new RsoAuthenticationException("Authentication error!");
        }
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler(RsoAuthenticationException.class)
    @ResponseBody
    public ClientErrorResponseJSON rsoAuthenticationExceptionHandler(Exception exception) {
        logger.error("Exception catched: ", exception);

        return new ClientErrorResponseJSON(403, exception.getMessage());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(RsoBadRequestException.class)
    @ResponseBody
    public ClientErrorResponseJSON rsoBadRequestExceptionHandler(Exception exception) {
        logger.error("Exception catched: ", exception);

        return new ClientErrorResponseJSON(400, exception.getMessage());
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ClientErrorResponseJSON exceptionHandler(Exception exception) {
        logger.error("Exception catched: ", exception);

        return new ClientErrorResponseJSON(500, exception.getMessage());
    }
}
