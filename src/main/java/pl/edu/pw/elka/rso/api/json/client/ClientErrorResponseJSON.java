package pl.edu.pw.elka.rso.api.json.client;

/**
 * Obiekt reprezentujący błąd, który ma być zwrócony do klienta.
 *
 * @author Michał Cybulski
 */
public class ClientErrorResponseJSON {

    private Integer errorCode;
    private String errorMessage;

    public ClientErrorResponseJSON() {
        // na potrzeby Jacksona
    }

    public ClientErrorResponseJSON(Integer errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClientErrorResponseJSON)) return false;

        ClientErrorResponseJSON that = (ClientErrorResponseJSON) o;

        if (errorCode != null ? !errorCode.equals(that.errorCode) : that.errorCode != null) return false;
        return !(errorMessage != null ? !errorMessage.equals(that.errorMessage) : that.errorMessage != null);

    }

    @Override
    public int hashCode() {
        int result = errorCode != null ? errorCode.hashCode() : 0;
        result = 31 * result + (errorMessage != null ? errorMessage.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ClientErrorResponseJSON{" +
                "errorCode=" + errorCode +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
