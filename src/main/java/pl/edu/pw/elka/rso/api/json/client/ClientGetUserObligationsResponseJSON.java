package pl.edu.pw.elka.rso.api.json.client;

import javax.validation.constraints.NotNull;

/**
 * Proste POJO mapowane do JSON z odpowiedzią dla przypadku użycia:
 * 1. Pobranie informacji o miesięcznych zobowiązaniach finansowych.
 *
 * @author Michał Cybulski
 */
public class ClientGetUserObligationsResponseJSON {

    @NotNull
    private Integer value;

    public ClientGetUserObligationsResponseJSON() {
        // pusty
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClientGetUserObligationsResponseJSON)) return false;

        ClientGetUserObligationsResponseJSON that = (ClientGetUserObligationsResponseJSON) o;

        return !(value != null ? !value.equals(that.value) : that.value != null);

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ClientGetUserObligationsResponseJSON{" +
                "value=" + value +
                '}';
    }
}
