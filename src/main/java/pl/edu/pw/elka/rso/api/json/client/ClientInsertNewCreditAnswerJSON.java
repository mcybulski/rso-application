package pl.edu.pw.elka.rso.api.json.client;

import javax.validation.constraints.NotNull;

/**
 * Proste POJO mapowane do JSON z odpowiedzią dla przypadku użycia:
 * 2. Wprowadzanie informacji o nowych kredytach.
 *
 * @author Michał Cybulski
 */
public class ClientInsertNewCreditAnswerJSON {

    @NotNull
    private String creditId;

    public ClientInsertNewCreditAnswerJSON() {
        // nic nie robimy
    }

    public String getCreditId() {
        return creditId;
    }

    public void setCreditId(String creditId) {
        this.creditId = creditId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClientInsertNewCreditAnswerJSON)) return false;

        ClientInsertNewCreditAnswerJSON that = (ClientInsertNewCreditAnswerJSON) o;

        return !(creditId != null ? !creditId.equals(that.creditId) : that.creditId != null);

    }

    @Override
    public int hashCode() {
        return creditId != null ? creditId.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ClientInsertNewCreditAnswerJSON{" +
                "creditId='" + creditId + '\'' +
                '}';
    }
}
