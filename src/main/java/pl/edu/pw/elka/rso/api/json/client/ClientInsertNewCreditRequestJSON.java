package pl.edu.pw.elka.rso.api.json.client;

import pl.edu.pw.elka.rso.api.json.common.ClientDataJSON;
import pl.edu.pw.elka.rso.api.json.common.CreditDataJSON;
import pl.edu.pw.elka.rso.api.json.data.DataInsertNewCreditRequestJSON;

import javax.validation.constraints.NotNull;

/**
 * Proste POJO mapowane do JSON z requestem dla przypadku użycia:
 * 2. Wprowadzanie informacji o nowych kredytach.
 *
 * @author Michał Cybulski
 */
public class ClientInsertNewCreditRequestJSON {

    @NotNull
    private ClientDataJSON client;
    @NotNull
    private CreditDataJSON credit;

    public ClientInsertNewCreditRequestJSON() {
        // pusty - na potrzeby Jacksona
    }

    public void setClient(ClientDataJSON client) {
        this.client = client;
    }

    public void setCredit(CreditDataJSON credit) {
        this.credit = credit;
    }

    public ClientDataJSON getClient() {
        return client;
    }

    public CreditDataJSON getCredit() {
        return credit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientInsertNewCreditRequestJSON that = (ClientInsertNewCreditRequestJSON) o;

        if (client != null ? !client.equals(that.client) : that.client != null) return false;
        if (credit != null ? !credit.equals(that.credit) : that.credit != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = client != null ? client.hashCode() : 0;
        result = 31 * result + (credit != null ? credit.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ClientInsertNewCreditRequestJSON{" +
                "client=" + client +
                ", credit=" + credit +
                '}';
    }
}
