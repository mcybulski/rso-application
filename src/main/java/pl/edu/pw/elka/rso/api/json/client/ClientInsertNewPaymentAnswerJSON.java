package pl.edu.pw.elka.rso.api.json.client;

import javax.validation.constraints.NotNull;

/**
 * Proste POJO mapowane do JSON z odpowiedzią dla przypadku użycia:
 * 3. Wprowadzanie informacji o spłacaniu kredytów.
 *
 * @author Michał Cybulski
 */
public class ClientInsertNewPaymentAnswerJSON {

    @NotNull
    private Integer remaining;

    public ClientInsertNewPaymentAnswerJSON() {
        // nic nie robimy
    }

    public Integer getRemaining() {
        return remaining;
    }

    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClientInsertNewPaymentAnswerJSON)) return false;

        ClientInsertNewPaymentAnswerJSON that = (ClientInsertNewPaymentAnswerJSON) o;

        return !(remaining != null ? !remaining.equals(that.remaining) : that.remaining != null);

    }

    @Override
    public int hashCode() {
        return remaining != null ? remaining.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ClientInsertNewPaymentAnswerJSON{" +
                "remaining=" + remaining +
                '}';
    }
}
