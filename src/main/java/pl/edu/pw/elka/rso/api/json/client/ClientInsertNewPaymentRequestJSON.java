package pl.edu.pw.elka.rso.api.json.client;

import pl.edu.pw.elka.rso.api.json.common.PaymentDataJSON;
import pl.edu.pw.elka.rso.api.json.data.DataInsertNewPaymentRequestJSON;

import javax.validation.constraints.NotNull;

/**
 * Proste POJO mapowane do JSON z requestem dla przypadku użycia:
 * 3. Wprowadzanie informacji o spłacaniu kredytów.
 *
 * @author Michał Cybulski
 */
public class ClientInsertNewPaymentRequestJSON {

    @NotNull
    private PaymentDataJSON payment;
    @NotNull
    private String paymentName;

    public ClientInsertNewPaymentRequestJSON() {
        // pusty - na potrzeby Jacksona
    }

    public PaymentDataJSON getPayment() {
        return payment;
    }

    public void setPayment(PaymentDataJSON payment) {
        this.payment = payment;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClientInsertNewPaymentRequestJSON)) return false;

        ClientInsertNewPaymentRequestJSON that = (ClientInsertNewPaymentRequestJSON) o;

        if (payment != null ? !payment.equals(that.payment) : that.payment != null) return false;
        return !(paymentName != null ? !paymentName.equals(that.paymentName) : that.paymentName != null);

    }

    @Override
    public int hashCode() {
        int result = payment != null ? payment.hashCode() : 0;
        result = 31 * result + (paymentName != null ? paymentName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ClientInsertNewPaymentRequestJSON{" +
                "payment=" + payment +
                ", paymentName='" + paymentName + '\'' +
                '}';
    }
}
