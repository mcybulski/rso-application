package pl.edu.pw.elka.rso.api.json.common;

/**
 * Informacje o nowej płatności.
 *
 * @author Michał Cybulski
 */
public class PaymentDataJSON {

    private Integer value;
    private String creditId;

    public PaymentDataJSON() {
        // pusty - na potrzeby Jacksona
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getCreditId() {
        return creditId;
    }

    public void setCreditId(String creditId) {
        this.creditId = creditId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentDataJSON that = (PaymentDataJSON) o;

        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (creditId != null ? !creditId.equals(that.creditId) : that.creditId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "PaymentData{" +
                "value=" + value +
                "creditId=" + creditId +
                "}";
    }
}