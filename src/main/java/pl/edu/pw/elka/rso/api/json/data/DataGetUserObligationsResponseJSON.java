package pl.edu.pw.elka.rso.api.json.data;

/**
 * Proste POJO mapowane do JSON z odpowiedzią dla przypadku użycia:
 * 1. Pobranie informacji o miesięcznych zobowiązaniach finansowych.
 *
 * @author Michał Cybulski
 */
public class DataGetUserObligationsResponseJSON {

    private Integer value;
    private String errorCode;

    public DataGetUserObligationsResponseJSON() {
        // pusty - na potrzeby Jacksona
    }

    public DataGetUserObligationsResponseJSON(Integer value, String errorCode) {
        this.value = value;
        this.errorCode = errorCode;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Integer getValue() {
        return value;
    }

    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataGetUserObligationsResponseJSON that = (DataGetUserObligationsResponseJSON) o;

        if (errorCode != null ? !errorCode.equals(that.errorCode) : that.errorCode != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (errorCode != null ? errorCode.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ClientGetUserObligationsResponseJSON{" +
                "value=" + value +
                ", errorCode='" + errorCode + '\'' +
                '}';
    }
}
