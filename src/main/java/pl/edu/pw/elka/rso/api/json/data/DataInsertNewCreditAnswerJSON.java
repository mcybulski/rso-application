package pl.edu.pw.elka.rso.api.json.data;

/**
 * Proste POJO mapowane do JSON z odpowiedzią dla przypadku użycia:
 * 2. Wprowadzanie informacji o nowych kredytach.
 *
 * @author Michał Cybulski
 */
public class DataInsertNewCreditAnswerJSON {

    private String creditId;
    private String errorCode;

    public DataInsertNewCreditAnswerJSON() {
        // pusty - na potrzeby Jacksona
    }

    public DataInsertNewCreditAnswerJSON(String creditId, String errorCode) {
        this.creditId = creditId;
        this.errorCode = errorCode;
    }

    public void setCreditId(String creditId) {
        this.creditId = creditId;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getCreditId() {
        return creditId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataInsertNewCreditAnswerJSON that = (DataInsertNewCreditAnswerJSON) o;

        if (creditId != null ? !creditId.equals(that.creditId) : that.creditId != null) return false;
        if (errorCode != null ? !errorCode.equals(that.errorCode) : that.errorCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = creditId != null ? creditId.hashCode() : 0;
        result = 31 * result + (errorCode != null ? errorCode.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ClientInsertNewCreditAnswerJSON{" +
                "creditId='" + creditId + '\'' +
                ", errorCode='" + errorCode + '\'' +
                '}';
    }
}
