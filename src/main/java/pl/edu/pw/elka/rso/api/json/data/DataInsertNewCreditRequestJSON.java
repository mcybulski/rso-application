package pl.edu.pw.elka.rso.api.json.data;

import pl.edu.pw.elka.rso.api.json.common.ClientDataJSON;
import pl.edu.pw.elka.rso.api.json.common.CreditDataJSON;

import javax.validation.constraints.NotNull;

/**
 * Proste POJO mapowane do JSON z requestem dla przypadku użycia:
 * 2. Wprowadzanie informacji o nowych kredytach.
 *
 * @author Michał Cybulski
 */
public class DataInsertNewCreditRequestJSON {

    @NotNull
    private String uid;
    @NotNull
    private String hash;
    @NotNull
    private ClientDataJSON client;
    @NotNull
    private CreditDataJSON credit;

    public DataInsertNewCreditRequestJSON() {
        // pusty - na potrzeby Jacksona
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public ClientDataJSON getClient() {
        return client;
    }

    public void setClient(ClientDataJSON client) {
        this.client = client;
    }

    public CreditDataJSON getCredit() {
        return credit;
    }

    public void setCredit(CreditDataJSON credit) {
        this.credit = credit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataInsertNewCreditRequestJSON)) return false;

        DataInsertNewCreditRequestJSON that = (DataInsertNewCreditRequestJSON) o;

        if (uid != null ? !uid.equals(that.uid) : that.uid != null) return false;
        if (hash != null ? !hash.equals(that.hash) : that.hash != null) return false;
        if (client != null ? !client.equals(that.client) : that.client != null) return false;
        return !(credit != null ? !credit.equals(that.credit) : that.credit != null);

    }

    @Override
    public int hashCode() {
        int result = uid != null ? uid.hashCode() : 0;
        result = 31 * result + (hash != null ? hash.hashCode() : 0);
        result = 31 * result + (client != null ? client.hashCode() : 0);
        result = 31 * result + (credit != null ? credit.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DataInsertNewCreditRequestJSON{" +
                "uid='" + uid + '\'' +
                ", hash='" + hash + '\'' +
                ", client=" + client +
                ", credit=" + credit +
                '}';
    }
}
