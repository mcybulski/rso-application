package pl.edu.pw.elka.rso.api.json.data;

/**
 * Proste POJO mapowane do JSON z odpowiedzią dla przypadku użycia:
 * 3. Wprowadzanie informacji o spłacaniu kredytów.
 *
 * @author Michał Cybulski
 */
public class DataInsertNewPaymentAnswerJSON {

    private Integer remaining;
    private String errorCode;

    public DataInsertNewPaymentAnswerJSON() {
        // pusty - na potrzeby Jacksona
    }

    public DataInsertNewPaymentAnswerJSON(Integer remaining, String errorCode) {
        this.remaining = remaining;
        this.errorCode = errorCode;
    }

    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Integer getRemaining() {
        return remaining;
    }

    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataInsertNewPaymentAnswerJSON that = (DataInsertNewPaymentAnswerJSON) o;

        if (errorCode != null ? !errorCode.equals(that.errorCode) : that.errorCode != null) return false;
        if (remaining != null ? !remaining.equals(that.remaining) : that.remaining != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = remaining != null ? remaining.hashCode() : 0;
        result = 31 * result + (errorCode != null ? errorCode.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ClientInsertNewPaymentAnswerJSON{" +
                "remaining=" + remaining +
                ", errorCode='" + errorCode + '\'' +
                '}';
    }
}
