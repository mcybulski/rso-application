package pl.edu.pw.elka.rso.api.json.data;

import pl.edu.pw.elka.rso.api.json.common.PaymentDataJSON;

import javax.validation.constraints.NotNull;

/**
 * Proste POJO mapowane do JSON z requestem dla przypadku użycia:
 * 3. Wprowadzanie informacji o spłacaniu kredytów.
 *
 * @author Michał Cybulski
 */
public class DataInsertNewPaymentRequestJSON {

    @NotNull
    String uid;
    @NotNull
    String hash;
    @NotNull
    private PaymentDataJSON payment;

    public DataInsertNewPaymentRequestJSON() {
        // pusty - na potrzeby Jacksona
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public PaymentDataJSON getPayment() {
        return payment;
    }

    public void setPayment(PaymentDataJSON payment) {
        this.payment = payment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataInsertNewPaymentRequestJSON)) return false;

        DataInsertNewPaymentRequestJSON that = (DataInsertNewPaymentRequestJSON) o;

        if (uid != null ? !uid.equals(that.uid) : that.uid != null) return false;
        if (hash != null ? !hash.equals(that.hash) : that.hash != null) return false;
        return !(payment != null ? !payment.equals(that.payment) : that.payment != null);

    }

    @Override
    public int hashCode() {
        int result = uid != null ? uid.hashCode() : 0;
        result = 31 * result + (hash != null ? hash.hashCode() : 0);
        result = 31 * result + (payment != null ? payment.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DataInsertNewPaymentRequestJSON{" +
                "uid='" + uid + '\'' +
                ", hash='" + hash + '\'' +
                ", payment=" + payment +
                '}';
    }
}
