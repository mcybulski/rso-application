package pl.edu.pw.elka.rso.api.json.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Klasa wyjątku związanego z błędem uwierzytelniania.
 *
 * @author Michał Cybulski
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class RsoAuthenticationException extends RuntimeException {

    public RsoAuthenticationException(String message) {
        super(message);
    }

}
