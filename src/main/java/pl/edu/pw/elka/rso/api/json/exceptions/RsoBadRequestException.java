package pl.edu.pw.elka.rso.api.json.exceptions;

/**
 * Wyjątek związany ze złym requestem.
 *
 * @author Michał Cybulski
 */
public class RsoBadRequestException extends RuntimeException {

    public RsoBadRequestException(String message) {
        super(message);
    }

}
