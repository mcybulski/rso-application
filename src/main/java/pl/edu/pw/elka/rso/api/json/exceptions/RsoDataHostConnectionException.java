package pl.edu.pw.elka.rso.api.json.exceptions;

/**
 * Wyjątek symbolizujący problem z połączeniem z węzłami warstwy danych.
 *
 * @author Michał Cybulski
 */
public class RsoDataHostConnectionException extends RuntimeException {

    public RsoDataHostConnectionException(String message) {
        super(message);
    }

}
