package pl.edu.pw.elka.rso.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Konfiguracja połączeń aplikacji.
 *
 * @author Michał Cybulski
 */
@Configuration
public class ConnectionConfiguration {

    @Value("${rso.connection.timeout}")
    private String CONNECTION_TIMEOUT;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(clientHttpRequestFactory());
    }

    private ClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        int timeout = Integer.parseInt(CONNECTION_TIMEOUT);
        factory.setReadTimeout(timeout);
        factory.setConnectTimeout(timeout);

        return factory;
    }
}
