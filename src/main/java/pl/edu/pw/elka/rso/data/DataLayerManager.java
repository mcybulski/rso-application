package pl.edu.pw.elka.rso.data;

import pl.edu.pw.elka.rso.api.json.data.*;

/**
 * Komponent łączący się z API warstwy danych.
 *
 * @author Michał Cybulski
 */
public interface DataLayerManager {

    /**
     * Wywouje metodę getUserObligations na API warstwy danych i zwraca rezultat.
     *
     * @param pesel numer PESEL klienta
     * @return rezultat zwrócony przez warstwę danych
     */
    DataGetUserObligationsResponseJSON getUserObligations(String pesel);

    /**
     * Wywołuje metodę insertNewCredit na warstwie danych i zwraca rezultat.
     *
     * @param dataInsertNewCreditRequestJSON zapytanie do warstwy danych
     * @return rezultat zwrócony przez warstwę danych
     */
    DataInsertNewCreditAnswerJSON insertNewCredit(DataInsertNewCreditRequestJSON dataInsertNewCreditRequestJSON);

    /**
     * Wywołuje metodę insertNewPayment na API warstwy danych i zwraca rezultat.
     *
     * @param dataInsertNewPaymentRequestJSON zapytanie do warstwy danych
     * @param userPesel numer PESEL płatnika
     * @return odpowiedź warstwy danych
     */
    DataInsertNewPaymentAnswerJSON insertNewPayment(DataInsertNewPaymentRequestJSON dataInsertNewPaymentRequestJSON,
                                                String userPesel);
}
