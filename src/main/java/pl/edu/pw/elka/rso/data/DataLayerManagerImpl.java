package pl.edu.pw.elka.rso.data;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import pl.edu.pw.elka.rso.api.json.data.*;
import pl.edu.pw.elka.rso.api.json.exceptions.RsoBadRequestException;
import pl.edu.pw.elka.rso.api.json.exceptions.RsoDataHostConnectionException;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Domyślna implementacja {@link DataLayerManager}.
 *
 * @author Michał Cybulski
 */
@Component
public class DataLayerManagerImpl implements DataLayerManager {

    private static final Logger logger = LoggerFactory.getLogger(DataLayerManagerImpl.class);

    // stan
    /**
     * Adresy API warstwy danych oddzielone średnikami.
     */
    @Value("${rso.data.hosts}")
    private String rsoDataHostsString;

    /**
     * Węzły warstwy danych.
     */
    private List<String> dataHosts;

    // zależności
    @Autowired
    private RestTemplate restTemplate;

    @PostConstruct
    public void init() {
        ImmutableList.Builder<String> builder = new ImmutableList.Builder<>();
        for (String dataHost : StringUtils.split(rsoDataHostsString, ";")) {
            builder.add(dataHost);
        }
        dataHosts = builder.build();
    }

    @Override
    public DataGetUserObligationsResponseJSON getUserObligations(String pesel) {
        String endpointUrl = "/app/obligations/" + pesel + "/";

        return (DataGetUserObligationsResponseJSON) getForObject(endpointUrl, DataGetUserObligationsResponseJSON.class);
    }

    @Override
    public DataInsertNewCreditAnswerJSON insertNewCredit(DataInsertNewCreditRequestJSON dataInsertNewCreditRequestJSON) {
        String endpointUrl = "/app/obligations/";

        return (DataInsertNewCreditAnswerJSON) postForObject(endpointUrl,
                dataInsertNewCreditRequestJSON,
                DataInsertNewCreditAnswerJSON.class);
    }

    @Override
    public DataInsertNewPaymentAnswerJSON insertNewPayment(DataInsertNewPaymentRequestJSON dataInsertNewPaymentRequestJSON,
                                                       String userPesel) {
        String endpointUrl = "/app/new-payment/" + userPesel + "/";

        return (DataInsertNewPaymentAnswerJSON) postForObject(endpointUrl,
                dataInsertNewPaymentRequestJSON,
                DataInsertNewPaymentAnswerJSON.class);
    }

    /**
     * Wykonuje zapytanie POST pod podany adres url na jednym z hostów warsty danych.
     *
     * @param url url
     * @param postObject obiekt JSON
     * @param returnClass spodziewana klasa wynikowa
     * @return obiekt wynikowy
     */
    private Object postForObject(String url, Object postObject, Class returnClass) {
        Object returnedObject = null;

        for (String hostUrl : getDatahostListForRequest()) {
            String endpointUrl = hostUrl + url;

            boolean success = false;
            try {
                returnedObject = restTemplate.postForObject(endpointUrl, postObject, returnClass);
                success = true;
            } catch(HttpClientErrorException | HttpServerErrorException e) {
                logger.warn("Failed to get object {}. Post parameters: endpoint: {}, object: {}. Exception: {}. Response: {}",
                        returnClass, endpointUrl, postObject, e, e.getResponseBodyAsString());
                if (400 == e.getStatusCode().value()) {
                    // bad request
                    throw new RsoBadRequestException(e.getMessage());
                }
            } catch (Exception e) {
                logger.warn("Failed to get object {}. Post parameters: endpoint: {}, object: {}. Exception: {}",
                        returnClass, endpointUrl, postObject, e);
            }

            if (success) {
                break;
            }
        }

        if (returnedObject == null) {
            throw new RsoDataHostConnectionException("Can't get proper response from any of the data nodes");
        }

        return returnedObject;
    }

    /**
     * Wykonuje zapytanie GET pod podany url na jednym z węzłów warstwy danych.
     *
     * @param url url
     * @param returnClass spodziewana klasa wynikowa
     * @return obiekt wynikowy
     */
    private Object getForObject(String url, Class returnClass) {
        Object returnedObject = null;

        for (String hostUrl : getDatahostListForRequest()) {
            String endpointUrl = hostUrl + url;

            boolean success = false;
            try {
                returnedObject = restTemplate.getForObject(endpointUrl, returnClass);
                success = true;
            } catch(HttpClientErrorException e) {
                logger.warn("Failed to get object {}. Post parameters: endpoint: {}. Exception: {}",
                        returnClass, endpointUrl, e);
                if (400 == e.getStatusCode().value()) {
                    // bad request
                    throw new RsoBadRequestException(e.getMessage());
                }
            } catch (Exception e) {
                logger.warn("Failed to get object {}. Post parameters: endpoint: {}. Exception: {}",
                        returnClass, endpointUrl, e);
            }

            if (success) {
                break;
            }
        }

        if (returnedObject == null) {
            throw new RsoDataHostConnectionException("Can't get proper response from any of the data nodes");
        }

        return returnedObject;
    }

    /**
     * Zwraca adresy hostów warstwy danych.
     *
     * @return adresy hostów warstwy danych
     */
    private List<String> getDatahostListForRequest() {
        Random random = new Random();

        List<String> randomlyShuffeledHostList = Lists.newArrayList(dataHosts);
        Collections.shuffle(randomlyShuffeledHostList, random);

        return randomlyShuffeledHostList;
    }
}
