package pl.edu.pw.elka.rso.security;

/**
 * Manager uwierzytelniający klientów.
 *
 * @author Michał Cybulski
 */
public interface SecurityManager {

    /**
     * Sprawdza czy podany token jest prawidłowy dla danego klienta i timestampa. Timestamp nie może być zbyt stary.
     *
     * @param token token
     * @param clientId identyfikator klienta
     * @param timestamp timestamp podany przez klienta
     * @return true jeśli token jest prawidłowy, false w przeciwnym przypadku
     */
    boolean isTokenValid(String token, String clientId, long timestamp);
}
