package pl.edu.pw.elka.rso.security;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.edu.pw.elka.rso.util.TimeManager;

import javax.annotation.PostConstruct;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/**
 * Domyślna implementacja {@link SecurityManager}.
 *
 * @author Michał Cybulski
 */
@Component
public class SecurityManagerImpl implements SecurityManager {

    private static final Logger logger = LoggerFactory.getLogger(SecurityManagerImpl.class);

    /**
     * Okienko tolerancji starości timestampa, wyrażone w milisekundach.
     */
    @Value("${rso.security.token_timestamp_window}")
    private long TOKEN_TIMESTAMP_WINDOW;
    /**
     * Identyfikatory i hasła klientów w formacie:
     * (...) klient:secret;klient:secret (...)
     */
    @Value("${rso.security.clients_secrets}")
    private String clientsSecrets;

    // stan
    /**
     * Mapa zawierająca identyfikatory i hasła klientów.
     */
    private Map<String, String> clientIdToSecretMap;

    // zależności
    @Autowired
    private TimeManager timeManager;

    @PostConstruct
    public void init() {
        ImmutableMap.Builder<String, String> builder = new ImmutableMap.Builder<>();
        for (String clientSecret : StringUtils.split(clientsSecrets, ";")) {
            String clientId = clientSecret.substring(0, clientSecret.indexOf(":"));
            String secret = clientSecret.substring(clientSecret.indexOf(":") + 1, clientSecret.length());
            builder.put(clientId, secret);
        }
        clientIdToSecretMap = builder.build();
    }

    @Override
    public boolean isTokenValid(String token, String clientId, long timestamp) {
        logger.trace("Trying to validate token '{}' for clientId '{}' and timestamp '{}'",
                token, clientId, timestamp);

        String clientSecret = clientIdToSecretMap.get(clientId);
        if (StringUtils.isEmpty(clientSecret)) {
            logger.trace("Token '{}' is invalid. There is no secret for clientId '{}'",
                    token, clientId);

            return false;
        }

        if (Math.abs(System.currentTimeMillis() - timestamp) > TOKEN_TIMESTAMP_WINDOW) {
            logger.trace("Token '{}' is invalid. Timestamp is too distant from current time",
                    token);

            return false;
        }

        boolean isValid = generateValidToken(clientSecret, timestamp).equals(token);

        return isValid;
    }

    /**
     * Generuje prawidłowy token dla danego sekretu i timestampa.
     *
     * @param secret sekret
     * @param timestamp timestamp
     * @return prawidłowy token
     */
    String generateValidToken(String secret, long timestamp) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        String concat = secret + Long.toString(timestamp);
        messageDigest.update(concat.getBytes());
        byte[] digest = messageDigest.digest();

        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : digest) {
            stringBuilder.append(String.format("%02x", b & 0xff));
        }

        return  stringBuilder.toString();
    }
}
