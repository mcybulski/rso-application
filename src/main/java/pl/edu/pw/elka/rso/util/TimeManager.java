package pl.edu.pw.elka.rso.util;

/**
 * Komponent zajmujący się czasem.
 *
 * @author Michał Cybulski
 */
public interface TimeManager {

    /**
     * Zwraca liczbę milisekund od unix epoch.
     *
     * @return liczba milisekund od unix epoch
     */
    long currentTimeMillis();
}
