package pl.edu.pw.elka.rso.util;

import org.springframework.stereotype.Component;

/**
 * Domyślna implementacja {@link TimeManager}.
 *
 * @author Michał Cybulski
 */
@Component
public class TimeManagerImpl implements TimeManager {

    @Override
    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }
}
