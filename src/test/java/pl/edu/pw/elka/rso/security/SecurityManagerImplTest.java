package pl.edu.pw.elka.rso.security;

import junit.framework.TestCase;

/**generateValidToken
 * Created by michal on 6/9/15.
 */
public class SecurityManagerImplTest extends TestCase {

    public void testGenerateValidToken() {
        SecurityManagerImpl securityManagerImpl = new SecurityManagerImpl();

        assertEquals("7a4421105788fc0b6478bf491b635cb2",
                securityManagerImpl.generateValidToken("secret", 1433954286748L));
    }
}